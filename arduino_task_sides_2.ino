#include <SPI.h>
#include <EEPROM.h>
#include <SD.h>
#include <Adafruit_VS1053.h>
#include "CmdMessenger.h"


#define SHIELD_RESET  -1      // VS1053 reset pin (unused!)
#define SHIELD_CS     7      // VS1053 chip select pin (output)
#define SHIELD_DCS    6      // VS1053 Data/command select pin (output)

// These are common pins between breakout and shield:
#define CARDCS 4     // Card chip select pin
// DREQ should be an Int pin, see http://arduino.cc/en/Reference/attachInterrupt
#define DREQ 3       // VS1053 Data request, ideally an Interrupt pin

const int BAUD_RATE = 9600;
const int calibrationPin = 4; // Jumper pin. Vcc on calibrationPin == calibration; GND on calibrationPin == task.
// Task and calibration LEDS. Red LED is pin 8; white LED is pin 9:
const int calibrationLEDPin = 8;
const int taskLEDPin = 9;
int memoryAddress = 0; // Calibration values will be saved at 0x00 (L) and 0x01 (R).
int calPinValue = 0;
char box[] = "BOX5"; // Update this variable for each one of the boxes; this name will be the one stored in the calibration log.
boolean calibration = false;

int side;
unsigned long duration;

/* Commands: */
enum {
    play_sound1,
    play_sound2,
    device_ready,
    error,
    send_volume,
    cmd_box,
    save_data
}; 

 /* Initialize messenger: */
CmdMessenger c = CmdMessenger(Serial,',',';','/');

/* Initialize music shield: */
Adafruit_VS1053_FilePlayer musicPlayer = 
  Adafruit_VS1053_FilePlayer(SHIELD_RESET, SHIELD_CS, SHIELD_DCS, DREQ, CARDCS);

/* callback */
void on_play_Sound2(void){
    musicPlayer.playFullFile("track002.mp3");
}

/* callback */
void on_play_Sound1(void){
    musicPlayer.playFullFile("track001.mp3");
}

/* callback */
void on_unknown_command(void){
    c.sendCmd(error,"Command without callback.");
}

/* callback */
void on_save_EEPROM(void){
   int calibratedVolume = c.readBinArg<int>();    
   
   if (side == 0){
      // We calibrated L and R, save both: 
      EEPROM.put(memoryAddress, calibratedVolume);
      memoryAddress += sizeof(int);
      EEPROM.put(memoryAddress, calibratedVolume);
   } else if (side == 1){
      // We only calibrated L side, save it at address = 0x00:
      EEPROM.put(memoryAddress, calibratedVolume);
   } else {
      // We only calibrated R side, save it at address = 0x01:
      memoryAddress += sizeof(int);
      EEPROM.put(memoryAddress, calibratedVolume);
   }

   memoryAddress = 0;
}

/* callback */
void on_send_volume(void){
   int volume = c.readBinArg<int>();
   side = c.readBinArg<int>();    
   if (side == 0){
      musicPlayer.setVolume(volume, volume);
   } else if (side == 1){
      musicPlayer.setVolume(volume, -1);
   } else {
      musicPlayer.setVolume(-1, volume);
   }
}


/* callback */
void send_box(void){
   c.sendCmd(cmd_box, box);
}

/* Attach callbacks for CmdMessenger commands */
void attach_callbacks(void) { 
    c.attach(play_sound1, on_play_Sound1);
    c.attach(play_sound2, on_play_Sound2);
    c.attach(send_volume, on_send_volume);
    c.attach(save_data, on_save_EEPROM);
    c.attach(on_unknown_command);
    c.attach(device_ready, send_box);
}

void error_blink(int color) {
  /* This function is called if there's an error during setup (shield or SD card not found).
  It causes one of the LEDs to start blinking. */
  int blinking_pin = 0;
  if(color == 1){
    blinking_pin = calibrationLEDPin;  
  } else {
    blinking_pin = taskLEDPin; 
  }
  while(1){
        digitalWrite(blinking_pin, HIGH);
        delay(500);
        digitalWrite(blinking_pin, LOW);
        delay(500);
  }
}

void setup() {
  
  pinMode(8, OUTPUT); // Red LED  
  pinMode(9, OUTPUT); // White LED

  Serial.begin(BAUD_RATE);
  if (!musicPlayer.begin()) { 
     error_blink(1);
  }
  if (!SD.begin(CARDCS)) {
    while (1);
    error_blink(2);  
  }
  
  /* Decide if we want to calibrate or not. If set for calibration, 
  calibrationPin will be connected to Vcc. */
  calPinValue  = analogRead(calibrationPin);
  if (calPinValue > 512){
    calibration = true;
    digitalWrite(calibrationLEDPin, HIGH);
  } else {
    digitalWrite(taskLEDPin, HIGH);  
  } 
     
}

void loop() {
  if( calibration == true ){
    /* -------------- Calibration code goes here ----------------------- */
    attach_callbacks(); 
    while(1){
        c.feedinSerialData(); // During calibration, just process commands coming from the serial port.
     }
  } else {
     /* -------------- Task code goes here ----------------------- */
     /* First recover the calibrated volume values */
     int volume_R;
     int volume_L;
     EEPROM.get(memoryAddress, volume_L);
     memoryAddress += sizeof(int);
     EEPROM.get(memoryAddress, volume_R);
     musicPlayer.setVolume(volume_L, volume_R);
     memoryAddress = 0;
     /* And enter the task: */
     while(1){
        duration = pulseIn(5, HIGH,100000000);
        Serial.println(duration);
        switch(duration) {
            case 300 ... 500:         
              Serial.println(F("Playing track 001"));
              musicPlayer.setVolume(volume_L, -1);
              musicPlayer.playFullFile("track001.mp3");
               break;
            case 700 ... 1000:
              Serial.println(F("Playing track 002"));
              musicPlayer.setVolume(-1, volume_R);
              musicPlayer.playFullFile("track001.mp3");
              break;
            case 1 ... 200:
              Serial.println(F("Stop playing"));
              musicPlayer.stopPlaying();
              break;              
            case 0:
              digitalWrite(LED_BUILTIN, HIGH); delay(1000);
              digitalWrite(LED_BUILTIN, LOW);  delay(1000);
              break; 
        }
        delay(100);
    }
  }
}
